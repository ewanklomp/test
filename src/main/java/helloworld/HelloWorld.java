package helloworld;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloWorld {

    public static void main(String[] args) {
		SpringApplication.run(HelloWorld.class, args);
        System.out.println("Hello, IT4IT");
        int test = 4;
        telOp(test);
        int test2 = 5;
        vermenigvuldig(test,test2);
    }
    public static int telOp (int test){

        test = test +1;
        System.out.println(test);
        return test;
    }

    public static int vermenigvuldig (int test, int test2){

        int res = test * test2;
        System.out.println(res);
        return res;
    }

}
